//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/mesh/xms/ElementSizing.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/VoidItem.h"

#include "smtk/model/Model.h"
#include "smtk/model/EntityRef.h"
#include "smtk/model/EntityIterator.h"

namespace smtk
{
namespace mesh
{
namespace xms
{

ElementSizing::ElementSizing()
  : m_globalSizing(-1)
  , m_sizings()
  , m_bias()
  , m_reverseLoops(false)
{
}

void ElementSizing::setSizing(const smtk::model::EntityRef& e, double sizing)
{
  this->m_sizings[e] = sizing;
}

double ElementSizing::sizing(const smtk::model::EntityRef& e) const
{
  double size = this->m_globalSizing;
  auto location = this->m_sizings.find(e);
  if(location != this->m_sizings.end())
    {
    size = location->second;
    }
  return size;
}

void ElementSizing::setBias(const smtk::model::EntityRef& e, double bias)
{
  this->m_bias[e] = (bias < 0. ? 0. : (bias > 1. ? 1. : bias));
}

double ElementSizing::bias(const smtk::model::EntityRef& e) const
{
  double bias = 1.0; //xms default
  auto location = this->m_bias.find(e);
  if(location != this->m_bias.end())
    {
    bias = location->second;
    }
  return bias;
}

}
}
}
