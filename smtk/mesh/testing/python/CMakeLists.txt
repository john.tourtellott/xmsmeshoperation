set(pytests
)

if (TARGET smtkDiscreteSession)
  list(APPEND pytests
    generateFaceMesh
    )
endif()

if (TARGET smtkPolygonSession)
  list(APPEND pytests
    generatePolyFaceEdgeMesh
    )
endif()

set(smtk_module_dir ${smtk_DIR}/${SMTK_PYTHON_MODULEDIR})
if(NOT IS_ABSOLUTE ${smtk_module_dir})
  get_filename_component(smtk_module_dir
    ${PROJECT_BINARY_DIR}/${smtk_DIR}/${SMTK_PYTHON_MODULEDIR} ABSOLUTE)
endif()

set(pyenv
  ${PROJECT_BINARY_DIR}
  ${smtk_module_dir}
  $ENV{PYTHONPATH})
if (WIN32)
  string(REPLACE ";" "\;" pyenv "${pyenv}")
else ()
  string(REPLACE ";" ":" pyenv "${pyenv}")
endif()

set(pathenv)
if (WIN32)
  set(pathenv
    ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
    $ENV{PATH})
  string(REPLACE ";" "\;" pathenv "${pathenv}")
endif()

foreach (pytest ${pytests})
  add_test(
    NAME    ${pytest}Py
    COMMAND "${PYTHON_EXECUTABLE}" "${CMAKE_CURRENT_SOURCE_DIR}/${pytest}.py"
    --data-dir=${PROJECT_SOURCE_DIR}/data
    )

  set_tests_properties("${pytest}Py"
    PROPERTIES
    ENVIRONMENT "PYTHONPATH=${pyenv}"
    )
  if (pathenv)
    set_property(TEST "${pytest}Py" APPEND
      PROPERTY
        ENVIRONMENT "PATH=${pathenv}"
      )
  endif ()
endforeach ()
