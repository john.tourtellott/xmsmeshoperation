import os
import sys
import unittest

import smtk
import smtk.attribute
import smtk.io
import smtk.mesh
import smtk.model
import smtk.operation
import smtk.resource
import smtk.session.mesh
import smtk.session.polygon

import xmsmesh
import smtkxmsmesh

script_dir = os.path.abspath(os.path.dirname(__file__))
DATA_DIR =  os.path.join(script_dir, os.pardir, 'data')

class TestPolygonMesher(unittest.TestCase):
    """Uses xmsmesh to generate mesh from polygon model"""
    def setUp(self):
        self.res_manager = smtk.resource.Manager.create()
        self.op_manager = smtk.operation.Manager.create()

        # Register polygon session
        smtk.session.polygon.Registrar.registerTo(self.res_manager)
        smtk.session.polygon.Registrar.registerTo(self.op_manager)

        # Register mesh.xms session
        smtk.mesh.xms.Registrar.registerTo(self.op_manager)

        smtk.operation.Registrar.registerTo(self.op_manager)
        self.op_manager.registerResourceManager(self.res_manager)

    def load_model(self, filename):
        """Loads smtk file storing polygon resource with 1 model"""
        read_op = smtk.session.polygon.LegacyRead.create()
        if not read_op:
            raise RuntimeError('No read operator')

        read_op.parameters().find('filename').setValue(filename)
        read_result = read_op.operate()
        if read_result.findInt('outcome').value(0) != int(smtk.operation.Operation.SUCCEEDED):
          raise RuntimeError('Error reading input file')

        # Access the resource
        # FYI no py binding for smtk::polygon::Resource, but doesn't seem to be needed
        resource = smtk.model.Resource.CastTo(read_result.find('resource').value())

        # Access the model
        model_ents = resource.entitiesMatchingFlags(smtk.model.MODEL_ENTITY, True)
        model_ent = model_ents.pop()
        model = smtk.model.Model(resource, model_ent)

        self.resource = resource
        return model

    def generate_face_mesh(self, model):
        """Uses xmsmesh to generate the face mesh"""
        # Create the mesh operator
        mesh_op = smtk.mesh.xms.GenerateMesh.create()

        # Associate it to the loaded model
        associated = mesh_op.parameters().associateEntity(model)
        if not associated:
            raise RuntimeError('Associate returned false')

        # Execute the operation
        res = mesh_op.operate()

        # Test for success
        outcome = res.find('outcome').value()
        if outcome != int(smtk.operation.Operation.Outcome.SUCCEEDED):
            print(mesh_op.log().convertToString())
            raise RuntimeError('"generate mesh" operator failed, outcome: ', outcome)

        # Print an overview of the output mesh
        mesh_collection = res.find('resource').value()
        return mesh_collection


    def test_mesher(self):
        model_file = os.path.join(DATA_DIR, 'boxWithHole.smtk')
        print('loading model file', model_file)
        model = self.load_model(model_file)

        print('generating face mesh')
        mesh_collection  = self.generate_face_mesh(model)
        self.assertEqual(mesh_collection.points().size(), 12)
        self.assertEqual(mesh_collection.cells().size(), 10)

        mesh_filename = 'boxWithHole.2dm'
        print('exporting mesh file', mesh_filename)
        if not smtk.io.exportMesh(mesh_filename, mesh_collection):
            raise RuntimeError('Error exporting 2dm file')

if __name__ == '__main__':
    unittest.main()
